﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

    public NavMeshAgent agent;
    GameObject home1;
    GameObject home2;
    GameObject home3;
    GameObject home4;
    bool ranOnce;

	// Use this for initialization
	void Start ()
    {
        ranOnce = true;

        home1 = References.instance.home1;
        home2 = References.instance.home2;
        home3 = References.instance.home3;
        home4 = References.instance.home4;

        int randNumber = Random.Range(0, 4);

        switch (randNumber)
        {
            case 0:
                agent.SetDestination(home1.transform.position);
                break;
            case 1:
                agent.SetDestination(home2.transform.position);
                break;
            case 2:
                agent.SetDestination(home3.transform.position);
                break;
            case 3:
                agent.SetDestination(home4.transform.position);
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (ranOnce)
        {
            if (other.CompareTag("Home"))
            {
                Debug.Log(gameObject.name);
                print(other.gameObject.name);
                References.instance.UpdateScore(1);
                ranOnce = false;
                transform.position = References.instance.start.position;
                this.gameObject.SetActive(false);
            }
        }
    }
}
