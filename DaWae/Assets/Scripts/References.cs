﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class References : MonoBehaviour {

    public GameObject home1;
    public GameObject home2;
    public GameObject home3;
    public GameObject home4;

    public Text scoreText;
    public GameObject upgradePanel;

    public Transform start;

    [HideInInspector]
    public int score;

    public static References instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(this);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        scoreText.text = "Score: 0";
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void UpdateScore(int amount)
    {
        score += amount;
        scoreText.text = "Score: " + score;
    }
}
