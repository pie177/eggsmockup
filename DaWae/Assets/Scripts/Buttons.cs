﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

    public GameObject unit;
    public Transform spawn;
    public Transform startPos;
    public int unitNumber;

    [HideInInspector]
    public List<GameObject> units = new List<GameObject>();

    float speed;
    int upgradeNumber;
    bool upgrading;
    int counter;

    // Use this for initialization
    void Start ()
    {
        speed = 1;
        upgradeNumber = 1;
        upgrading = true;
        counter = 0;

        for(int i = 0; i < unitNumber; i++)
        {
            GameObject tempObj = Instantiate(unit, spawn.position, Quaternion.identity);
            units.Add(tempObj);
            tempObj.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void _SpawnMeme()
    {
        units[counter].transform.position = startPos.position;

        units[counter].SetActive(true);

        counter++;

        if(counter == 50)
        {
            counter = 0;
        }
    }

    public void _UpgradeSpeed()
    {
        if (References.instance.score >= 10 && upgradeNumber < 4)
        {
            upgradeNumber++;
            speed /= upgradeNumber;
        }
        References.instance.UpdateScore(-10);
    }

    public void _HoldDown()
    {
        InvokeRepeating("_SpawnMeme", 0f, speed);
    }

    public void _Release()
    {
        CancelInvoke("_SpawnMeme");
    }

    public void _UpgradeScreen()
    {
        References.instance.upgradePanel.SetActive(upgrading);
        upgrading = !upgrading;
    }

}
